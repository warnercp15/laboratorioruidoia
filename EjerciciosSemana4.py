import cv2
from matplotlib import pyplot as plt
import os.path as path
import numpy as np
exist=False
img=''
ruta=''
def menu():
    global  img,ruta,exist

    try:
        opcion=input("\n*Ejercicios Semana 4 RP By Warner & Fabián*"
                     "\n1)Load Image"
                     "\n2)Color Balance"
                     "\n3)Blue Screen Matting"
                     "\n4)Efects"
                     "\n5)Histogram Equalization"
                      "\n6)High-Quality Image Resampling"
                     "\n7)Salir"
                     "\n\nDigite una opción: ")

        if opcion == "1":
            ruta = "imagenes/"

            print("\n*NOTA*: La imagen debe estar en la carpeta de imágenes del proyecto")
            print("*NOTA*: Puede utilizar el siguiente nombre: imagen1.jpeg")

            ruta += input("\nNombre de la imagen: ")
            if (path.exists(ruta)):
                exist=True
                img = cv2.imread(ruta)  # cargar imagen
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # Se configura bgr a rgb la img original
                plt.subplot(111), plt.imshow(img), plt.title('Original'), plt.xticks([]), plt.yticks([])
                print("\n*Imagen cargada exitosamente*")
            else:
                print("\n*Esta ruta es incorrecta*")
                menu()

        elif opcion == "2":
            if exist == False:
                print("\n*Cargue una imagen con la opción 1*")
                menu()
            alpha = 1.0
            beta = 0
            alpha = float(input('\nDigite un valor de contraste [1.0-3.0]: '))
            beta = int(input('Digite un valor de brillo [0-100]: '))
            new_image = cv2.convertScaleAbs(img, alpha=alpha, beta=beta)

            plt.subplot(121), plt.imshow(img), plt.title('Original'), plt.xticks([]), plt.yticks([])
            plt.subplot(122), plt.imshow(new_image), plt.title('Color Balance'), plt.xticks([]), plt.yticks([])

        elif opcion == "3":
            if exist == False:
                print("\n*Cargue una imagen con la opción 1*")
                menu()

            font = cv2.imread("imagenes/azul.jpeg")
            font = cv2.cvtColor(font, cv2.COLOR_BGR2RGB)
            plt.subplot(222), plt.imshow(font), plt.title('Blue Example'), plt.xticks([]), plt.yticks([])

            image_copy = np.copy(font)
            lower_blue = np.array([0, 0, 100])
            upper_blue = np.array([120, 100, 255])
            mask = cv2.inRange(image_copy, lower_blue, upper_blue)

            mask2 = cv2.cvtColor(mask, cv2.COLOR_BGR2RGB)
            plt.subplot(223), plt.imshow(mask2), plt.title('Mask'), plt.xticks([]), plt.yticks([])

            masked_image = np.copy(image_copy)
            masked_image[mask != 0] = [0, 0, 0]

            background_image = cv2.imread(ruta)
            width = font.shape[1]
            height = font.shape[0]
            dim = (width, height)
            resized = cv2.resize(background_image, dim)
            crop_background = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)

            crop_background[mask == 0] = [0, 0, 0]
            final_image = crop_background + masked_image

            resized = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)

            plt.subplot(221), plt.imshow(resized), plt.title('Original'), plt.xticks([]), plt.yticks([])
            plt.subplot(224), plt.imshow(final_image), plt.title('Blue Screen Matting'), plt.xticks([]), plt.yticks(
                [])

        elif opcion == "4":
            if exist == False:
                print("\n*Cargue una imagen con la opción 1*")
                menu()
            subOpcion = input("\n*Efectos disponibles*"
                              "\n1)Gaussian Blur"
                              "\n2)Gray Scale"
                              "\n3)Blue Shift"
                               "\n4)Sobel X and Sobel Y Filters"
                              "\n5)Detected Edges"
                              "\n\nDigite una opción: ")

            plt.subplot(121), plt.imshow(img), plt.title('Original'), plt.xticks([]), plt.yticks([])

            if subOpcion == "1":
                gauss = cv2.GaussianBlur(img, (17, 17), 0)
                plt.subplot(122), plt.imshow(gauss), plt.title('Gaussian Blur'), plt.xticks([]), plt.yticks([])

            elif subOpcion == "2":
                gray = cv2.imread(ruta, 0)
                dst0 = cv2.cvtColor(gray, cv2.COLOR_BGR2RGB)
                plt.subplot(122), plt.imshow(dst0), plt.title('Gray Scale'), plt.xticks(
                    []), plt.yticks([])

            elif subOpcion == "3":
                dst0 = cv2.imread(ruta)
                plt.subplot(122), plt.imshow(dst0), plt.title('Blue Shift'), plt.xticks(
                    []), plt.yticks([])

            elif subOpcion == "4":
                gray = cv2.imread(ruta, 0)
                dst0 = cv2.cvtColor(gray, cv2.COLOR_BGR2RGB)
                frame = cv2.Sobel(dst0, -1, dx=1, dy=0, ksize=11, scale=1, delta=0, borderType=cv2.BORDER_DEFAULT)
                dst1 = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                plt.subplot(122), plt.imshow(dst1), plt.title('Sobel X and Sobel Y Filters'), plt.xticks(
                    []), plt.yticks([])

            elif subOpcion == "5":
                edge_img = cv2.Canny(img, 100, 200)
                dst1 = cv2.cvtColor(edge_img, cv2.COLOR_BGR2RGB)
                plt.subplot(122), plt.imshow(dst1), plt.title('Detected Edges'), plt.xticks(
                    []), plt.yticks([])

            else:
                print("*Opción inválida*")
                menu()

        elif opcion == "5":
            if exist == False:
                print("\n*Cargue una imagen con la opción 1*")
                menu()

            ni = cv2.imread(ruta, 0)
            dst0=cv2.cvtColor(ni, cv2.COLOR_BGR2RGB)
            plt.subplot(121), plt.imshow(dst0), plt.title('Gray Scale'), plt.xticks(
                []), plt.yticks([])

            clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
            cl1 = clahe.apply(ni)
            dst2 = cv2.cvtColor(cl1, cv2.COLOR_BGR2RGB)

            plt.subplot(122), plt.imshow(dst2), plt.title('Histogram Equalization'), plt.xticks(
                []), plt.yticks([])

        elif opcion == "6":
            if exist == False:
                print("\n*Cargue una imagen con la opción 1*")
                menu()

            ni = cv2.imread(ruta, cv2.IMREAD_UNCHANGED)

            scale_percent = 220  # percent of original size
            width = int(ni.shape[1] * scale_percent / 100)
            height = int(ni.shape[0] * scale_percent / 100)
            dim = (width,height)
            resized = cv2.resize(ni, dim, interpolation=cv2.INTER_AREA)

            width = img.shape[1]+int(img.shape[1]/2)
            height = ni.shape[0]  # keep original height
            dim = (width, height)
            # resize image
            resized2 = cv2.resize(ni, dim, interpolation=cv2.INTER_AREA)

            width = ni.shape[1]  # keep original width
            height = img.shape[0]+int(img.shape[0]/2)
            dim = (width, height)
            # resize image
            resized3 = cv2.resize(ni, dim, interpolation=cv2.INTER_AREA)

            cv2.imshow("Original", ni)
            cv2.imshow("Resize width and height", resized)
            cv2.imshow("Resize only width", resized2)
            cv2.imshow("Resize only height", resized3)

            cv2.waitKey(0)
            cv2.destroyAllWindows()

        elif opcion == "7":
            exit()

        else:
            print("Opción inválida")
            menu()

        plt.show()
        menu()

    except ValueError as error:
        print("\n*Error inesperado*")
        print(error)
        menu()

menu()