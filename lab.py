import cv2
from matplotlib import pyplot as plt
import matplotlib.patches as patches
import numpy as np
import os.path as path
img = ""
exist=False
def menu():
    global  img,exist
    try:
        opcion=input("\n*Laboratorio #1 RP By Warner & Fabián*"
                     "\n1)Obtener RGB"
                     "\n2)Extraer region"
                     "\n3)Redimensionar sin aspecto"
                     "\n4)Redimensionar con aspecto"
                     "\n5)Rotar"
                     "\n6)Suavisar"
                     "\n7)Cargar imagen"
                     "\n9)Blue Screen Matting"
                     "\n10)Color Balance"
                     "\n8)Salir"
                     "\n\nDigite una opción: ")
        if opcion=="1":
            if exist==False:
                print("\n*Cargue una imagen con la opción 7*")
                menu()

            height = img.shape[0]
            width = img.shape[1]
            print("\n*NOTA*: La coordenada x no puede exceder: " + str(width-1))
            print("*NOTA*: La coordenada y no puede exceder: " + str(height-1))

            x=int(input("\nCoordenada x:"))
            y=int(input("Coordenada y:"))
            img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)  # Se configura bgr original para obtener el color correcto
            b, g, r= (img[y, x])
            print("\nRed:"+str(r)+"\nGreen:"+str(g)+"\nBlue:"+str(b))
            fig2 = plt.figure()
            ax2 = fig2.add_subplot(122, aspect='equal')
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # Se configura rgb a bgr de la img original
            plt.title('RGB'), plt.xticks([]), plt.yticks([])
            ax2.add_patch(
                patches.Rectangle(
                    (0,0),
                    1,
                    1,
                    facecolor='#%02x%02x%02x' % (r,g,b),
                    fill=True
                ))
            plt.subplot(121), plt.imshow(img), plt.title('Original'), plt.xticks([]), plt.yticks([])

        elif opcion=="2":
            if exist == False:
                print("\n*Cargue una imagen con la opción 7*")
                menu()

            height = img.shape[0]
            width = img.shape[1]
            print("\n*NOTA*: La coordenada x no puede exceder: " + str(width-1))
            print("*NOTA*: La coordenada y no puede exceder: " + str(height-1))

            x1 = int(input("\nCoordenada x1:"))
            y1 = int(input("Coordenada y1:"))

            print("\n*NOTA*: Las siguientes 2 coordenadas deben ser mayores a las anteriores respectivamente")

            x2 = int(input("\nCoordenada x2:"))
            y2 = int(input("Coordenada y2:"))
            roi = img[y1:y2, x1:x2]  #se toma la region de acuerdo al cuadro que se forma con los puntos dados
            plt.subplot(121), plt.imshow(img), plt.title('Original'), plt.xticks([]), plt.yticks([])
            plt.subplot(122), plt.imshow(roi), plt.title('ROI'), plt.xticks([]), plt.yticks([])

        elif opcion == "3":
            if exist == False:
                print("\n*Cargue una imagen con la opción 7*")
                menu()
            w = int(input("\nNuevo ancho:"))
            h = int(input("Nueva altura:"))
            resized = cv2.resize(img, (w, h))
            plt.subplot(121), plt.imshow(img), plt.title('Original'), plt.xticks([]), plt.yticks([])
            plt.subplot(122), plt.imshow(resized), plt.title('Redimensionada sin aspecto'), plt.xticks([]), plt.yticks(
                [])

        elif opcion == "4":
            if exist == False:
                print("\n*Cargue una imagen con la opción 7*")
                menu()
            w = int(input("\nNuevo Ancho:"))
            h = int(input("Nueva Altura:"))
            r = 300.0 / w
            dim = (300, int(h * r))
            resized = cv2.resize(img, dim)
            plt.subplot(121), plt.imshow(img), plt.title('Original'), plt.xticks([]), plt.yticks([])
            plt.subplot(122), plt.imshow(resized), plt.title('Redimensionada con aspecto'), plt.xticks([]), plt.yticks(
                [])

        elif opcion == "5":
            if exist == False:
                print("\n*Cargue una imagen con la opción 7*")
                menu()
            grados = int(input("\nGrados de rotacion:"))
            rows, cols = img.shape[:2]
            M = cv2.getRotationMatrix2D((cols / 2, rows / 2), grados, 1)
            rotate = cv2.warpAffine(img, M, (cols, rows))
            plt.subplot(121), plt.imshow(img), plt.title('Original'), plt.xticks([]), plt.yticks([])
            plt.subplot(122), plt.imshow(rotate), plt.title('Rotacion'), plt.xticks([]), plt.yticks([])

        elif opcion == "6":
            if exist == False:
                print("\n*Cargue una imagen con la opción 7*")
                menu()
            gauss = cv2.GaussianBlur(img, (17, 17), 0)
            plt.subplot(121), plt.imshow(img), plt.title('Original'), plt.xticks([]), plt.yticks([])
            plt.subplot(122), plt.imshow(gauss), plt.title('Gaussian Blur'), plt.xticks([]), plt.yticks([])

        elif opcion == "7":
            ruta = "imagenes/"

            print("\n*NOTA*: La imagen debe estar en la carpeta de imagenes del proyecto")
            print("*NOTA*: Puede utilizar el siguiente nombre: imagen1.jpeg")

            ruta += input("\nNombre de la imagen: ")
            if (path.exists(ruta)):
                exist=True
                img = cv2.imread(ruta)  # cargar imagen
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # Se configura bgr a rgb la img original
                plt.subplot(111), plt.imshow(img), plt.title('Original'), plt.xticks([]), plt.yticks([])
                print("\n*Imagen cargada exitosamente*")
            else:
                print("\n*Esta ruta es incorrecta*")
                menu()

        elif opcion == "8":
            exit()
        else:
            print("*Opción inválida*")
            menu()
        plt.show()
        menu()
    except ValueError:
        print("*Error inesperado*")
        menu()
menu()